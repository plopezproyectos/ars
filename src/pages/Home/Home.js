import { useEffect } from "react";
import Banner from "../../components/Banner/Banner";
import CssDark from "../../components/CssDark"
import Grid from "../../components/Grid/Grid";
import "./Home.css";

export default function Home({ ampliar, setAmpliar, moverMas, moverMenos, adelantar, setAdelantar, renderizar }) {

    CssDark(true)

    return (
        <div className="homeContainer">
            <Banner />
            <div className="home-works-cont">
                <div className="Home-works">
                    WORKS
                </div>
                <div className="home-works-links">
                    <div className="home-works-button">ALL</div>
                    <div className="home-works-button">PORTRAITS</div>
                    <div className="home-works-button">UI UX DESIGN</div>
                    <div className="home-works-button">BRANDING</div>

                </div>

</div>
                <Grid ampliar={ampliar} setAmpliar={setAmpliar} moverMas={moverMas} moverMenos={moverMenos} adelantar={adelantar} setAdelantar={setAdelantar} renderizar={renderizar} />
            </div>
        
            )
}