import React from 'react'
import "./AboutDos.css"
import CssDark from "../../components/CssDark";

let logos = [
    {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },
      {
        img: "ddc.jpg"
      },

]

const AboutDos = () => {

    CssDark(true)

    return (
        <div className='about-container'>
            <div className='about-titulo-principal'>

                <p className='about-titulo-principal-p'> WE'RE A </p>
                <p className='about-titulo-principal-p'> CREATIVE </p>
                <p className='about-titulo-principal-p'> STUDIO</p>
            </div>
            <div className='about-text-cont'>
                <div className='about-text'>
                    ARS is a widely experienced studio dedicated to provide High-End Visualizations.
                </div>
                <div className='about-text'>
                    For over 15 years we have supported architects, construction firms, real state developers and advertisement agencies around the world.
                </div>
                <div className='about-text'>
                    We focus on research and developing cutting-edge techniques to archieve the excellence in our visualizations.
                </div>
                <div className='about-text'>
                    Our Services: Architectural Visualizations / Computer-generated images (CGI) / Photomontages / Animations and fly-through / Commercial 3d plants and sections.
                </div>
                <div className='about-text'>
                    The quality and compromise of our work is very important to us, in all projects, ARS is committed to producing work of the highest quality for its clients.
                </div>
            </div>
            <hr className='about-hr'></hr>
            <div className='about-titulo-principal'>
                <div className='about-titulo-principal-p'>
                    WHAT WE DO
                </div>
            </div>
            <div>
                <div className='about-text-cont-dos'>
                    <p className='about-text-dos'>WE </p>
                    <p className='about-text-dos'>CREATE A </p>
                    <p className='about-text-dos'>BEAUTIFULL AND</p>
                    <p className='about-text-dos'>UNIQUE </p>
                    <p className='about-text-dos'>VISUAL </p>
                    <p className='about-text-dos'>SOLUTIONS </p>
                </div>
                <div className='about-text-cont-dos'>
                    <p className='about-text-dos'>FOR</p>
                    <p className='about-text-dos'>ARCHITECTURE AND </p>
                    <p className='about-text-dos'>REAL </p>
                    <p className='about-text-dos'>STATE </p>
                    <p className='about-text-dos'>MARKET </p>
                </div>
            </div>
            <div className='about-text-cont'>
                <div>
                    <div className='about-text-services'>
                        Services
                    </div>
                    <div className='about-text-tres'>
                        Exterior Images
                    </div>
                    <div className='about-text-tres'>
                        Interior Images
                    </div>
                    <div className='about-text-tres'>
                        VR360
                    </div>
                    <div className='about-text-tres'>
                        Movies
                    </div>
                    <div className='about-text-tres'>
                        Aerial Photography
                    </div>
                    <div className='about-text-tres'>
                        Photomontages
                    </div>                    
                </div>

            </div>
            <hr className='about-hr'></hr>
            <div>
                <p>PARTNERS</p>
            </div>
            <div className='about-cont-logos'>
                {
                    
                    logos.map((imglog)=>(
                        <>
                        <img src = {imglog.img} className = 'about-img-logos' />                    
                        </>

                    ))
                }
            </div>
        </div>
    )
}

export default AboutDos