import { useEffect } from "react";
import CssDark from "../../components/CssDark";
import "./About.css";

export default function About() {

CssDark(false)

    return(
        <>

<div className="services-head">
                    ABOUT US
                    <hr className="services-hr"></hr>
                </div>
                                    <div className="services-title">
                                    TEAM
                                    </div>
                                    <div className="services-desc">
                                    ARS is a widely experienced studio dedicated to provide High-End Visualizations.

For over 15 years we have supported architects, construction firms, real state developers and advertisement agencies around the world.

We focus on research and developing cutting-edge techniques to achieve the excellence in our visualizations.

Our Services: Architectural Visualization / Computer-generated images (CGI) / Photomontages / Animations and fly-through / Commercial 3d plants and sections.

The quality and compromise of our work is very important to us, in all projects, ARS is committed to producing work of the highest quality for its clients

ARS es un estudio con amplia experiencia, dedicado a proporcionar visualizaciones de alta calidad.

Durante más de 15 años hemos apoyado a arquitectos, empresas de construcción, desarrolladores inmobiliarios y agencias de publicidad en todo el mundo.

Nos centramos en la investigación y el desarrollo de técnicas de vanguardia para lograr la excelencia en nuestras visualizaciones.

La calidad y el compromiso de nuestro trabajo es muy importante para nosotros, en todos los proyectos, ARS se compromete a producir trabajos de la más alta calidad para sus clientes.
                                    </div >
                                    <div className="about-team">
                                   <div className="about-box">
                                        <img className="about-img" src ="https://www.ars-estudio.com/img/imgs/team1.jpg"/>
                                        <div className="about-name">ALVARO GENTILE</div>
                                        <div className="about-cargo">Architect | CG-Artist</div>
                                    </div>  
                                    <div className="about-box">
                                        <img className="about-img" src="https://www.ars-estudio.com/img/imgs/team3.jpg"/>
                                        <div className="about-name">FABIAN CARDOZO</div>
                                        <div className="about-cargo">3d Modeler | CGArtist</div>
                                    </div>
                                    <div className="about-box" >
                                        <img className="about-img" src ="https://www.ars-estudio.com/img/imgs/team2.jpg"/>
                                        <div className="about-name">SILVINA GUARDIA</div>
                                        <div className="about-cargo">Manager account</div>
                                    </div>
                                    </div>
 
        </>
    )
}