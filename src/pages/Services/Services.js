import CssDark from "../../components/CssDark"
import './Services.css'

export default function Services() {

    CssDark(false)

    let serv = [
        {
            img:"https://www.ars-estudio.com/img/work/zoom/51.jpg",
            title:"EXTERIORS AND INTERIOR IMAGES",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        },
        {
            img:"https://www.ars-estudio.com/img/work/zoom/32.jpg",
            title:"STILL-MOVIE IMAGES",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        },
        {
            img:"https://www.ars-estudio.com/img/work/zoom/43.jpg",
            title:"AERIAL IMAGES",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        },
        {
            img:"https://www.ars-estudio.com/img/work/zoom/44.jpg",
            title:"ANIMATIONS MOVIES",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        }
        ,
        {
            img:"https://www.ars-estudio.com/img/work/zoom/44.jpg",
            title:"VR360",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        },
        {
            img:"https://www.ars-estudio.com/img/work/zoom/44.jpg",
            title:"AERIAL PHOTOGRAPHY",
            text:"Give lady of the such they sure it. Me contained explained my education. Vulgar as hearts by garret. Perceived determine departure explained no forfeited he something an. Contrasted dissimilar get joy you instrument out reasonably"
        }
    ]
    return(
        <>
            <div className="services-container">
                <div className="services-head">
                    SERVICES
                    <hr className="services-hr"></hr>
                </div>
                {serv.map((servi, index)=>
                    <>
                        {((index + 1) % 2) === 0 ? (

                    <div key={index} className="services-box-right">                            
                                <div className="services-text">
                                    <div className="services-title">
                                    {servi.title} 
                                    </div>
                                    <div className="services-desc">
                                    {servi.text} 
                                    </div>
                                
                                </div>
                                <img src={servi.img} className = "services-img" data-aos="fade-right"/>
                            
                            </div>
                        ):(

                            <div key={index} className="services-box-left"> 
                                <div className="services-text">
                                    <div className="services-title">
                                    {servi.title} 
                                    </div>
                                    <div className="services-desc">
                                    {servi.text} 
                                    </div>
                                
                                </div>
                                <img src={servi.img} className = "services-img" data-aos="fade-left"/>
                            </div>

                        )
                        
                        }
 

                    </>
                )}               
            </div>
            </>
    )
}