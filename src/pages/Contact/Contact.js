import { useEffect } from "react";
import CssDark from "../../components/CssDark"
import './Contact.css'

export default function Contact() {


CssDark(false)

    return(
        <>
            <div className="contact-container">
                <div className="contact-main-title">
                    GET IN TOUCH
                </div>
                <div className="contact-box">
                    <div className="contact-title">
                      SEND MESSAGE  
                    </div>
                    
                    <textarea placeholder="Your Name" className="contact-text-box"/>
                    <textarea placeholder="Your Mail" className="contact-text-box"/>
                    <textarea placeholder="Your Message" rows="5" className="contact-text-box"/>
                    <button className="contact-button">SEND MESSAGE</button>
                </div>
            </div>
        </>
    )
}