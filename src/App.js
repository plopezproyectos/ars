import './App.css';
import './Dark.css'
import { lazy, Suspense, useEffect, useState } from 'react';
import Footer from './components/Footer/Footer';
import NavBar from './components/NavBar/NavBar';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import Works from './pages/Works/Works';
import About from './pages/about/About';
import Contact from './pages/Contact/Contact';
import Home from './pages/Home/Home';
import Services from './pages/Services/Services';
import Aos from "aos";
import "aos/dist/aos.css";

import Cursor from './components/Cursor/Cursor';
import { MainProvider } from './context/MainContext';
import AboutDos from './pages/AboutDos/AboutDos';
import Cargador from './components/Cargador/Cargador';

function App() {

  useEffect(function () {
    Aos.init({
      duration: 1000,
      once: true
    });
  }, []);

  let img = [
    {
      img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/43.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/44.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/13.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/10.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/11.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/12.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/13.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/50.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
      text: "Fullscreen/distortion/vertical"
    },
    {
      img: "https://www.ars-estudio.com/img/work/zoom/11.jpg",
      text: "Fullscreen/distortion/vertical"
    },
  ]

  const [ampliar, setAmpliar] = useState(0)

  const [renderizar, setRenderizar] = useState(0)

  const [adelantar, setAdelantar] = useState(true)

  function moverMas() {
    console.log("estoy en mover mas, ampliar es: " + ampliar + " adelantar es: " + adelantar)
    if (adelantar) {
      setAmpliar(cont => cont + 1)
    } else {
      setAmpliar(cont => cont)
    }
  }

  function moverMenos() {
    console.log("estoy en mover menos, ampliar es: " + ampliar)
    if (adelantar) {
      setAmpliar(cont => cont - 1)
    } else {
      setAmpliar(cont => cont)
    }
  }

  useEffect(() => {
    console.log("Ampliar cambio a: " + ampliar + " adelantar es: " + adelantar)

    if (ampliar < img.length) {
      if (ampliar > -1 ) {
        setRenderizar(ampliar)        
      }else{
        setAmpliar(renderizar)
      }
    }else{
      setAmpliar(renderizar)
    }

    if (ampliar == (img.length - 1)) {
      console.log("CAMBIO ADELANTAR A FALSE")
      setAdelantar(false)
    } else (
      setAdelantar(true)
    )

  }, [ampliar])

  return (

    <div className='App Dark'>
<Suspense fallback={<h1>Cargando...</h1>}>
      <MainProvider>
        <Router>
          <Cursor />
          <Cargador />
          <NavBar />

          <div>
            <Routes>

              <Route path="/works" element={<Works />} />

              <Route path="/services" element={<Services />} />

              <Route path="/about" element={<AboutDos />} /> 

              <Route path="/contact" element={<Contact />} />

              <Route path="/" element={<Home ampliar={ampliar} setAmpliar={setAmpliar} moverMas={moverMas} moverMenos={moverMenos} adelantar = {adelantar} setAdelantar={setAdelantar} renderizar = {renderizar}/>} />

              <Route path="*" element={<Home />} />

            </Routes>
          </div>

          <Footer />
        </Router>

      </MainProvider>
      </Suspense>
    </div>

  );
}





export default App;
