import { createContext, useState } from "react";

export const MainContext = createContext();

export const MainProvider = ({children}) => {

    const [mouseType, setMouseType] = useState(false)

    const [verNavBar, setVerNavBar] = useState(true)

    const [ampliar, setAmpliar] = useState(0)

    return (
        <MainContext.Provider value = {{
            mouseType,
            setMouseType,
            verNavBar, 
            setVerNavBar, 
            ampliar,
            setAmpliar
        }}>
            {children}    
        </MainContext.Provider>
    )

}