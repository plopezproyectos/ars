import { useState, useContext } from "react"
import { Link } from "react-router-dom"
import { MainContext } from "../../context/MainContext"
import './NavBar.css'

export default function NavBar() {

    const { mouseType, setMouseType, verNavBar } = useContext(MainContext)

    const [desplegado, setDesplegado] = useState(true)

    const [posicionScroll, setPoscionScroll] = useState()

    window.onscroll = function() {
        var y = window.scrollY;
        setPoscionScroll(y);
      };

    return (
        <>
            <div className="NavBar-cont-base">

            </div>
            <nav >


                <div className={desplegado ? "NavBar-cont-menu-desplegado" : "NavBar-cont-menu"}>
                   {!desplegado &&
                    <div className="box-NavBar">
                        <div className="menu-link-box">
                            <Link to="/" onClick={() => setDesplegado(!desplegado)} className="NavBar-link-decoration"><div className="NavBar-menu-link"><p className="NavBar-menu-link-p">HOME</p></div></Link>
                        </div>
                        <div className="menu-link-box">
                            <Link to="/services" onClick={() => setDesplegado(!desplegado)} className="NavBar-link-decoration"><div className="NavBar-menu-link"><p className="NavBar-menu-link-p">SERVICES</p></div></Link>
                        </div>
                        <div className="menu-link-box">
                            <Link to="/about" onClick={() => setDesplegado(!desplegado)} className="NavBar-link-decoration"><div className="NavBar-menu-link"><p className="NavBar-menu-link-p">ABOUT</p></div></Link>
                        </div>
                        <div className="menu-link-box">
                            <Link to="/contact" onClick={() => setDesplegado(!desplegado)} className="NavBar-link-decoration"><div className="NavBar-menu-link"><p className="NavBar-menu-link-p">CONTACT</p></div></Link>
                        </div>

                        <div className="navbar-redes">
                             <a href="https://www.facebook.com/ARS.estudio.web/" target="_blank"><img src="facebook.svg" className="navbar-logo-redes" /></a>
                             <a href="https://www.behance.net/ars-estudio" target="_blank"><img src="behance.svg" className="navbar-logo-redes" /></a>
                            <a href="https://www.instagram.com/ars_visualizations/" target="_blank"><img src="instagram.svg" className="navbar-logo-redes" /></a>
                        </div>

                    </div>                   
                   }
                   

                </div>
                <div className={verNavBar ? (posicionScroll < 100 ? "NavBar-cont-NavBar" : "NavBar-cont-NavBar-chico") : "NavBarNoVer"}>
                    <Link to="/"><img src="ars.svg" className={posicionScroll < 100 ? "App-logo" : "App-logo-chico"} alt="logo" />
                        <svg width="24px" height="24px" viewBox="0 0 24 24" color="var(--main-font-color)" xmlns="http://www.w3.org/2000/svg">
                            <path d="M 6343.54 8813.22 l 3178.64 0 l 0 3041 l -3189.95 -2.81 c -1583.16 -1.32 -1641 -3038.19 11.31 -3038.19 Z m -1834.58 -2376.77 l 3786.16 0 c 676.07 0 1268.03 253.81 1233.7 1010.28 l -3336.62 6.76 c -3058.35 169.4 -3149.93 5484.11 -176.17 5664.7 l 4772.33 0.9 l 0 -5765.98 c -75.13 -1302.25 -966.73 -2172.55 -2316.72 -2191.81 l -3963.46 6.34 l 0.78 1268.81 Z" />
                        </svg>
                    </Link>
                    <button onClick={() => setDesplegado(!desplegado)} className="menuButton-NavBar" onMouseLeave={() => setMouseType(true)} onMouseEnter={() => setMouseType(false)}
                    >
                        <div className={desplegado ? (!mouseType ? "menu-movil-arriba-corrido" : "menu-movil-arriba") : "menu-movil-arriba-x"}></div>
                        <div className={desplegado ? (!mouseType ? "menu-movil-medio-corrido" : "menu-movil-medio") : "menu-movil-medio-x"} ></div>
                        <div className={desplegado ? (!mouseType ? "menu-movil-abajo-corrido" : "menu-movil-abajo") : "menu-movil-abajo-x"} ></div>
                    </button>
                </div>






            </nav>
            <hr className="barra-NavBar"></hr>

        </>
    )
}