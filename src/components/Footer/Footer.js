import { Link } from "react-router-dom"
import "./Footer.css"

export default function Footer() {
    return (
        <>

            <div className="footer-container">
            <hr className="footer-hr" />
                <div className="footer-arriba">
                    <div className="footer-arriba-columnas">
                        <div className="footer-arriba-filas">
                            <div className="footer-titulo-grande">
                                READY TO VIEW
                            </div>
                            <div className="footer-titulo-grande">
                                THE FUTURE ...IN 3D
                            </div>
                            <div className="footer-titulo-chico">
                                <button className="footer-button-get">
                                <Link to="/contact" className="footer-button-link" >GET IN TOUCH</Link>
                                </button>
                                
                            </div>
                        </div>

                        <div className="footer-arriba-filas">
                            <div className="footer-titulo-chico">
                                Welcome to ARS
                            </div>
                            <div className="footer-texto">
                                We are a creative studio. We create beautiful ans unique visual solutions for Architecture and Real State Market.
                            </div>
                        </div>
                        <div className="footer-columna-arriba">
                            <div className="footer-arriba-filas">
                                <div className="footer-titulo-chico">
                                <Link to="/about">
                                    Services
                                </Link>
                                </div>
                                <div className="footer-texto">
                                    Exterior Images
                                </div>
                                <div className="footer-texto">
                                    Interior Images
                                </div>
                                <div className="footer-texto">
                                    VR360
                                </div>
                                <div className="footer-texto">
                                    Movies
                                </div>
                                <div className="footer-texto">
                                    Aerial Photography
                                </div>
                                <div className="footer-texto">
                                    Photomontages
                                </div>
                                <div className="footer-titulo-chico">

                                </div>
                            </div>
                            <div className="footer-arriba-filas">
                                <div className="footer-titulo-chico">
                                <Link to="/contact" >
                                    Studio                                    
                                </Link>

                                </div>
                                <div className="footer-texto">
                                    Services
                                </div>
                                <div className="footer-texto">
                                    About
                                </div>
                                <div className="footer-texto">
                                    Blog
                                </div>
                                <div className="footer-texto">
                                    Contacts
                                </div>
                                <div className="footer-titulo-chico">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <hr className="footer-hr" />
                <div className="footer-abajo">
                    <div className="footer-abajo-columnas">
                        <div>
                            <img src="ars.svg" className="footer-logo" alt="logo" />
                        </div>
                        <div className="se-ve-en-grande">
                            <div className="footer-titulo-chico">
                                Copyright © ARS 2022 . All Rights Reserved.
                            </div>                            
                        </div>

                        <div>
                             <a href="https://www.facebook.com/ARS.estudio.web/" target="_blank"><img src="facebook.svg" className="footer-redes" /></a>
                             <a href="https://www.behance.net/ars-estudio" target="_blank"><img src="behance.svg" className="footer-redes" /></a>
                            <a href="https://www.instagram.com/ars_visualizations/" target="_blank"><img src="instagram.svg" className="footer-redes" /></a>
                        </div>
                    </div>

                    <div className="se-ve-en-chico">
<div></div>
                            <div className="footer-titulo-chico">
                                Copyright © ARS 2022 . All Rights Reserved.
                            </div>                            
<div></div>
                    </div>
                </div>
            </div>
        </>
    )
}