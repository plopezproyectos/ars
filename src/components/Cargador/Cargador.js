import React from 'react';
import './Cargador.css'


const Cargador = () => {
    return (
        <div className='cargador-contenedor'>
            <div className='cargador-contenedor-secundario'>
                <div className='cargador-logo-cont'>
                    <img src="ars.svg" className='cargador-logo' />
                </div>

                <div className='cargador-text'>
                    READY TO VIEW
                </div>
                <div className='cargador-text'>
                    THE FUTURE ...IN 3D
                </div>                
            </div>


        </div>
    )
}

export default Cargador