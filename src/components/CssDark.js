import { useEffect } from "react";

export default function CssDark(prop) {
    useEffect(() => {
        if (prop) {

            const rootStyles = document.documentElement.style;
            rootStyles.setProperty("--main-background-color", "#262626");
            rootStyles.setProperty("--secundary-background-color", "#464646");
            rootStyles.setProperty("--main-font-color", "#eeece6");
            rootStyles.setProperty("--logo-filter", "invert(99%) sepia(4%) saturate(1528%) hue-rotate(332deg) brightness(109%) contrast(88%)");

        } else {
            const rootStyles = document.documentElement.style;
            rootStyles.setProperty("--main-background-color","#eeece6" );
            rootStyles.setProperty("--secundary-background-color", "#afaeae");
            rootStyles.setProperty("--main-font-color", "#262626");
            rootStyles.setProperty("--logo-filter", "invert(12%) sepia(9%) saturate(17%) hue-rotate(96deg) brightness(100%) contrast(92%)");
   
        };
    }, [])
};         