import { useState, useContext, useEffect } from "react"
import './Grid.css';
import { MainContext } from "../../context/MainContext"
import ProgressiveImage from "react-progressive-graceful-image";


export default function Grid( {ampliar, setAmpliar ,moverMas, moverMenos, adelantar, setAdelantar, renderizar}) {

    const { setVerNavBar } = useContext(MainContext)



    let img = [
        {
            img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/43.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/44.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/13.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/10.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/11.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/12.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/51.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/13.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/50.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/32.jpg",
            text: "Fullscreen/distortion/vertical"
        },
        {
            img: "https://www.ars-estudio.com/img/work/zoom/11.jpg",
            text: "Fullscreen/distortion/vertical"
        },
    ]
    const [handleShow, setHandleShow] = useState(false)

    const [windowSize, setWindowSize] = useState(getWindowSize());

    const [maximoImagenes, setMaximoImagenes] = useState(5)

    useEffect(()=>{
        if(img.length < 5){
            setMaximoImagenes(img.length)
        }else{
            setMaximoImagenes(5)
        }
    },[])

  function getWindowSize() {
    const {innerWidth, innerHeight} = window;
    return {innerWidth, innerHeight};
  }

    useEffect(() => {
      function handleWindowResize() {
        setWindowSize(getWindowSize());
      }
  
      window.addEventListener('resize', handleWindowResize);
  
      return () => {
        window.removeEventListener('resize', handleWindowResize);
      };
    }, []);

  
function cargarMas(){
    if ((maximoImagenes + 10) < img.length ) {
        setMaximoImagenes(img.length)
    }else{
        setMaximoImagenes(maximoImagenes + 10)
    }
    
}

    function destapar() {
        setVerNavBar(true)
        setHandleShow(false)
    }

    function tapar(key) {
        setVerNavBar(false)
        setHandleShow(true)
        setAmpliar(cost => cost - cost + key)
        console.log("Tapar es: " + key)
    }





    let tiempoEspera = true;

    function setearEspera() {
        setTimeout(() => {
            tiempoEspera = true
        }, 100);
    }

    let cargaUnaVez = true


    useEffect(() => {
        if (cargaUnaVez) {
        document.addEventListener('keydown', detectKeyDowm, true)            
        }
        cargaUnaVez = false
    }, [])

    const detectKeyDowm = (e) => {
        let teclaPresionada = e.key;
        console.log("La tecla presionada es => " + e.key + " ampliar es: " + ampliar)
        if (tiempoEspera) {
            if (teclaPresionada === "ArrowRight" && ampliar < img.length) {
                tiempoEspera = false
                moverMas()
                setearEspera()
            } else if (teclaPresionada === "ArrowLeft") {
                tiempoEspera = false
                moverMenos()
                setearEspera()
            }
        }
    }


    return (
        <>
            <div className="grid-container">
                {img.map((el, key) => (
                    (maximoImagenes > key) &&
                    
                    <div className="imagewrapper" key={key}>
                        <div className="grid-box" >



                            <ProgressiveImage src={el.img} placeholder={"ars.svg"}>
                                {(src, loading) => (
                                    <img
                                        src={src}
                                        data-aos="fade-up" className="grid-images" onClick={() => tapar(key)}
                                    />
                                )}
                            </ProgressiveImage>
                            




                            <div>{el.text}</div>
                        </div>
                    </div>
                ))}
            </div>

            {maximoImagenes < img.length && <button onClick={()=> cargarMas()} className="grid-see-more">See more...</button>}

            {(handleShow && windowSize.innerWidth > 900) &&
                <div className="grid-flotante">

                    <div className="grid-flotante-imagewrapper"  >
                        <div className="grid-flotante-tapar" onClick={() => destapar()}></div>
                        <button className="grid-flotante-flecha" onClick={() => moverMenos()}>

                            <div className="grid-flecha-caja-izquierda">
                               
                                <div className="grid-flecha-arriba-izquierda"></div>
                            </div>

                        </button>

                        <button onClick={() => destapar()} className="grid-cerrar-container">
                            <div className="grid-cerrar-arriba"></div>
                            <div className="grid-cerrar-abajo" ></div>
                        </button>

                        <div className="grid-flotante-box">
                            <img src={img[renderizar].img} className="grid-flotante-images" />
                            <div className="grid-flotante-texto">{img[renderizar].text}</div>
                        </div>


                        <button className="grid-flotante-flecha" onKeyDown={()=>alert("hola")} onClick={() => moverMas()}>

                            <div className="grid-flecha-caja-derecha">
                                <div className="grid-flecha-arriba-derecha"></div>
                                
                            </div>


                        </button>
                    </div>

                </div>
            }

        </>
    )
}