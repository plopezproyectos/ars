import React from 'react'

import { useState } from "react"
import { Link } from "react-router-dom"
import './NavBar.css'

const Nav = () => {

    const [desplegado, setDesplegado] = useState(false)

  return (
    <header>
        <nav>
        <Link to="/home"><img src="ars.svg" className="logo" alt="logo" />
                    <svg width="24px" height="24px" viewBox="0 0 24 24" color="var(--main-font-color)" xmlns="http://www.w3.org/2000/svg">
                        <path d="M 6343.54 8813.22 l 3178.64 0 l 0 3041 l -3189.95 -2.81 c -1583.16 -1.32 -1641 -3038.19 11.31 -3038.19 Z m -1834.58 -2376.77 l 3786.16 0 c 676.07 0 1268.03 253.81 1233.7 1010.28 l -3336.62 6.76 c -3058.35 169.4 -3149.93 5484.11 -176.17 5664.7 l 4772.33 0.9 l 0 -5765.98 c -75.13 -1302.25 -966.73 -2172.55 -2316.72 -2191.81 l -3963.46 6.34 l 0.78 1268.81 Z" />
                    </svg>
                </Link>
                <button onClick={() => setDesplegado(!desplegado)} className="menuButton-NavBar">
                    <img src="menu.svg" alt="logo" className="menuImg-NavBar" />
                </button>
        </nav>
    </header>
  )
}

export default Nav