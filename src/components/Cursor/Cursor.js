import React, { useContext, useEffect, useState } from 'react';
import { MainContext } from '../../context/MainContext';
import "./Cursor.css";

const Cursor = () => {

  const {mouseType} = useContext(MainContext)

useEffect(()=>{
let cursor = document.getElementById("elCursor");
document.addEventListener("mousemove", (e) =>{
    let x = e.clientX;
    let y = e.clientY;    
    cursor.style.position = "fixed";  
    cursor.style.zIndex = "12";  
    setTimeout(() => {
    cursor.style.left = x + "px"; 
    cursor.style.top = y + "px"; 
    cursor.style.pointerEvents= "none";             
    }, 100);
})
},[])

  

    const[cursorSobre, getCursorSobre] = useState(true)

  return (
    <>
     <div id ="elCursor">
        <div className={mouseType ? "cursorClass" : "cursorClassMenu"} >

        </div>
    </div>    
    
    
   
    </>

  )
}

export default Cursor